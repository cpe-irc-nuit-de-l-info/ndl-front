module.exports = {
  'publicPath': '',
  'outputDir': 'www/dist',
  'assetsDir': 'static',
  'transpileDependencies': [
    'vuetify'
  ]
}
