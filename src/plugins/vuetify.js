import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify)

export default new Vuetify({
  icons: {
    iconfont: 'fa'
  },
  theme: {
    themes: {
      light: {
        primary: '#2196f3',
        secondary: '#03a9f4',
        accent: '#00bcd4',
        error: '#f44336',
        warning: '#ff9800',
        info: '#3f51b5',
        success: '#4caf50'
      },
      dark: {
        primary: '#2196f3',
        secondary: '#03a9f4',
        accent: '#00bcd4',
        error: '#f44336',
        warning: '#ff9800',
        info: '#3f51b5',
        success: '#4caf50'
      }
    }
  }
})
