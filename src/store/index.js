import Vue from 'vue'
import Vuex from 'vuex'
import user from '@/store/modules/user'
import processes from '@/store/modules/processes'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    processes
  }
})
