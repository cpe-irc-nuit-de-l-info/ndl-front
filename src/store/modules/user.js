import axios from 'axios'
import router from '@/router'
export default {
  namespaced: true,
  state: {
    username: 'eric.sawayn@example.org',
    password: 'password',
    name: 'Sawayn',
    firstName: 'Eric',
    token: '',
    drawer: false
  },
  actions: {
    login ({ state, commit }) {
      axios.post('/oauth/token', {
        'grant_type': 'password',
        'client_id': '2',
        'client_secret': 'QSaYnfmaoRX8lNmseu608EciMaa3i4CMgJW8dRoE',
        'username': state.username,
        'password': state.password,
        'scope': ''
      })
        .then(({ data }) => {
          commit('setToken', data.access_token)
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + state.token
          axios.get('api/users')
            .then((data) => {
              state.firstName = data.data[0].first_name
              state.name = data.data[0].name
            })
        })
        .finally(() => router.push({ name: 'profile' }))
    }
  },
  mutations: {
    setDrawer (state, value) {
      state.drawer = value
    },
    setUsername (state, value) {
      state.username = value
    },
    setPassword (state, value) {
      state.password = value
    },
    setName (state, value) {
      state.name = value
    },
    setFirstName (state, value) {
      state.firstName = value
    },
    setToken (state, value) {
      state.token = value
    }
  }
}
