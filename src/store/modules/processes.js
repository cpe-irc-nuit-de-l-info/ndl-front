export default {
  namespaced: true,
  actions: {
    mailTo (context, { body }) {
      window.location = `mailto:?body=${body}`
    }
  }
}
